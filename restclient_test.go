package restclient

import (
	"context"
	"errors"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/entah/gotap"
	"gitlab.com/entah/restclienttest"
)

func TestCall(t *testing.T) {

	ass := assert.New(t)

	testServer := restclienttest.NewTestServer(t)
	defer testServer.Close()
	// simulate GET request with 200 http code and a response
	testServer.AppendHandler(
		http.MethodGet,
		"/json",
		restclienttest.TestRequest{
			Name:         "Success call",
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"success"}`),
		})

	client := testServer.Client()
	rclient := WrapClient(client)

	reqData := Req{
		Host:   testServer.URL,
		Path:   "/json",
		Method: http.MethodGet,
	}

	respBuff, respErr := rclient.Call(context.Background(), reqData)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(respBuff, "response should be not empty")
	ass.Equal([]byte(`{"status":"success"}`), respBuff, "response expected")

	// simulate GET request with 200 http code, query params, headers, and a response
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"success"}`),
		})

	reqData.Method = "POST"
	reqData.Queries = map[string][]string{"test": {"1"}, "method": {"post"}}
	reqData.BearerToken = "bearer-token"
	reqData.ContentType = "application/json"
	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(respBuff, "response should be not empty")
	ass.Equal([]byte(`{"status":"success"}`), respBuff, "response expected")
	_, code, _ := rclient.CallWithCode(context.Background(), reqData)
	ass.Equal(200, code, "http code should be 200")

	// simulate error when creating `http.Request` object.
	reqData.Body = math.NaN()
	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.Error(respErr, "request should be error because NaN cannot be marshalled to json")
	ass.Empty(respBuff, "response should be empty because error returned")

	reqData.Body = nil
	respBuff, respErr = rclient.Call(nil, reqData)
	ass.Error(respErr, "request should be error because nil `context.Context` supplied")
	ass.Empty(respBuff, "response should be empty because error returned")

	// simulate error when call `http.Client#Do` method.
	reqData.Host = "foobar"
	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.Error(respErr, "request should be error because invalid host supplied")
	ass.Empty(respBuff, "response should be empty because error returned")

	// simulate error when call server return non 2xx http code and `Non2xxError` set to true.
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 500,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte("something wrong"),
		})

	reqData.Host = testServer.URL
	reqData.Non2xxError = true
	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.Error(respErr, "request should be error because server responsed with non 2xx status code and our request `Non2xxError` set to true")
	ass.Empty(respBuff, "response should be empty because error returned")

	// simulate error when read body response.
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type":   {"application/json"},
				"Content-Length": {"1"},
			},
		})

	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.Error(respErr, "request should be error because server Content-Length more than zero but no body response ever received")
	ass.Empty(respBuff, "response should be empty because error returned")
}

func TestBind(t *testing.T) {

	ass := assert.New(t)

	testServer := restclienttest.NewTestServer(t)
	defer testServer.Close()
	// simulate GET request with 200 http code and a response
	testServer.AppendHandler(
		http.MethodGet,
		"/json",
		restclienttest.TestRequest{
			Name:         "Success call",
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"success"}`),
		})

	client := testServer.Client()
	rclient := WrapClient(client)

	reqData := Req{
		Host:   testServer.URL,
		Path:   "/json",
		Method: http.MethodGet,
	}

	type respbody struct {
		Status string `json:"status"`
	}

	var resp respbody

	respErr := rclient.Bind(context.Background(), reqData, &resp)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(resp, "response should be not empty")
	ass.Equal(respbody{Status: "success"}, resp, "response expected")

	// simulate GET request with 200 http code, query params, headers, and a response
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"SUCCESS"}`),
		})

	reqData.Method = "POST"
	reqData.Queries = map[string][]string{"test": {"1"}, "method": {"post"}}
	reqData.BearerToken = "bearer-token"
	reqData.ContentType = "application/json"
	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(resp, "response should be not empty")
	ass.Equal(respbody{Status: "SUCCESS"}, resp, "response expected")

	// overide bearer token via header `Authorization`
	reqData.Headers = map[string][]string{"Authorization": {"Bearer bearer-token"}}
	code, respErr := rclient.BindWithCode(context.Background(), reqData, &resp)
	ass.NoError(respErr, "request should be no error")
	ass.Equal(200, code, "http code should be 200")
	ass.Equal(respbody{Status: "SUCCESS"}, resp, "response expected")

	// use `BearerGen` function
	reqData.BearerToken = ""
	reqData.Headers = nil
	reqData.BearerGen = func() string { return "bearer-token" }
	code, respErr = rclient.BindWithCode(context.Background(), reqData, &resp)
	ass.NoError(respErr, "request should be no error")
	ass.Equal(200, code, "http code should be 200")
	ass.Equal(respbody{Status: "SUCCESS"}, resp, "response expected")

	// supply nil to expected body resp will discard the response.
	code, respErr = rclient.BindWithCode(context.Background(), reqData, nil)
	ass.NoError(respErr, "request should be no error")
	ass.Equal(200, code, "http code should be 200")
	ass.Equal(respbody{Status: "SUCCESS"}, resp, "response expected")

	// simulate error when discarding response
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type":   {"application/json"},
				"Content-Length": {"1"},
			},
		})

	code, respErr = rclient.BindWithCode(context.Background(), reqData, nil)
	ass.NoError(respErr, "request should be no error because error sent to log when discarding response")
	ass.Equal(200, code, "http code should be 200")

	// simulate error when read response body
	resp.Status = ""
	code, respErr = rclient.BindWithCode(context.Background(), reqData, &resp)
	ass.Error(respErr, "request should be error because we expect 1 byte sent to server, but nothing arrived")
	ass.Equal(0, code, "http code is zero since error returned")
	ass.Equal(respbody{}, resp, "empty response since no body response received")

	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"SUCCESS"}`),
		})

	// simulate error when creating `http.Request` object.
	reqData.Body = math.NaN()
	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.Error(respErr, "request should be error because NaN cannot be marshalled to json")
	ass.Empty(resp, "response should be empty because error returned")

	reqData.Body = nil
	respErr = rclient.Bind(nil, reqData, &resp)
	ass.Error(respErr, "request should be error because nil `context.Context` supplied")
	ass.Empty(resp, "response should be empty because error returned")

	// simulate error when call `http.Client#Do` method.
	reqData.Host = "foobar"
	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.Error(respErr, "request should be error because invalid host supplied")
	ass.Empty(resp, "response should be empty because error returned")

	// simulate error when call server return non 2xx http code and `Non2xxError` set to true.
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 500,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte("something wrong"),
		})

	reqData.Host = testServer.URL
	reqData.Non2xxError = true
	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.Error(respErr, "request should be error because server responsed with non 2xx status code and our request `Non2xxError` set to true")
	ass.Empty(resp, "response should be empty because error returned")

	type xmlbody struct {
		Status string `xml:"success"`
	}

	var xmlresp xmlbody

	// simulate binding xml response
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},
			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/xml"},
			},
			ResponseBody: []byte(`<xmlbody><success>success</success></xmlbody>`),
		})

	respErr = rclient.Bind(context.Background(), reqData, &xmlresp)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(xmlresp, "response should be not empty")
	ass.Equal(xmlbody{Status: "success"}, xmlresp, "response body expected")

	// simulate discarding response body if content type not supported (we only support json and xml)
	testServer.AppendHandler(
		http.MethodPost,
		"/json",
		restclienttest.TestRequest{
			Name: "Success call",
			Test: func(ass *assert.Assertions, r *http.Request) bool {
				buff, err := ioutil.ReadAll(r.Body)
				ass.NoError(err, "read body should be no error")
				ass.NotEmpty(buff, "body should be not empty")
				ass.Equal(`{"hello":"world"}`, string(buff), "body request should be expected")
				return true
			},
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return headers.Get("Content-Type") == "application/json" && headers.Get("Authorization") == "Bearer bearer-token"
			},

			QueryTest: func(ass *assert.Assertions, queries url.Values) bool {
				qTest := queries.Get("test")
				qMeth := queries.Get("method")
				ass.Equal("1", qTest)
				ass.Equal("post", qMeth)
				return true
			},
			ResponseCode: 200,
			ResponseBody: []byte(`<xmlbody><success>success</success></xmlbody>`),
		})

	xmlresp.Status = ""
	reqData.Body = `{"hello":"world"}`
	respErr = rclient.Bind(context.Background(), reqData, &xmlresp)
	ass.NoError(respErr, "request should be no error")
	ass.Empty(xmlresp, "response should be empty because error returned")

	// simulate send requst body in form of `io.Reader`
	reqData.Body = strings.NewReader(`{"hello":"world"}`)
	respErr = rclient.Bind(context.Background(), reqData, &xmlresp)
	ass.NoError(respErr, "request should be no error")
	ass.Empty(xmlresp, "response should be empty because error returned")

	// simulate send requst body in form of byte arrays
	reqData.Body = []byte(`{"hello":"world"}`)
	respErr = rclient.Bind(context.Background(), reqData, &xmlresp)
	ass.NoError(respErr, "request should be no error")
	ass.Empty(xmlresp, "response should be empty because error returned")

	// simulate send requst body in form of struct
	reqData.Body = struct {
		Hello string `json:"hello"`
	}{Hello: "world"}
	respErr = rclient.Bind(context.Background(), reqData, &xmlresp)
	ass.NoError(respErr, "request should be no error")
	ass.Empty(xmlresp, "response should be empty because error returned")

}

func TestMergeHeader(t *testing.T) {
	ass := assert.New(t)
	h1 := http.Header{"Header1": {"a"}, "Header3": {"a"}}
	h2 := http.Header{"Header1": {"b", "c"}, "Header2": {"a", "b"}, "Header3": {"b"}, "Header4": {"a"}}
	mergeHeader(h1, h2)
	ass.Equal(http.Header{"Header1": {"a", "b", "c"}, "Header2": {"a", "b"}, "Header3": {"a", "b"}, "Header4": {"a"}}, h1, "result must be equal to merged value")
}

func TestURLCreation(t *testing.T) {
	ass := assert.New(t)

	tt := []struct {
		name     string
		req      Req
		expected string
	}{
		{
			name: "plain path",
			req: Req{
				Host: "http://localhost",
				Path: "/v1/api/test",
			},
			expected: "http://localhost/v1/api/test",
		},
		{
			name: "path with query with single value",
			req: Req{
				Host:    "http://localhost",
				Path:    "/v1/api/test",
				Queries: map[string][]string{"test": {"1"}, "id": {"1"}},
			},
			expected: "http://localhost/v1/api/test?id=1&test=1",
		},
		{
			name: "path with query with multiple value",
			req: Req{
				Host:    "http://localhost",
				Path:    "/v1/api/test",
				Queries: map[string][]string{"test": {"1", "2"}, "id": {"1"}},
			},
			expected: "http://localhost/v1/api/test?id=1&test=1&test=2",
		},
		{
			name: "path with path params",
			req: Req{
				Host:   "http://localhost",
				Path:   "/v1/api/test/:id/:user",
				Params: map[string]string{"id": "1", "user": "foobar"},
			},
			expected: "http://localhost/v1/api/test/1/foobar",
		},
		{
			name: "path with path params and query params",
			req: Req{
				Host:    "http://localhost",
				Path:    "/v1/api/test/:id/:user",
				Queries: map[string][]string{"test": {"1", "2"}, "id": {"1"}},
				Params:  map[string]string{"id": "1", "user": "foobar"},
			},
			expected: "http://localhost/v1/api/test/1/foobar?id=1&test=1&test=2",
		},
	}

	for _, test := range tt {
		ass.Equal(test.expected, test.req.CompleteURL(), test.name)
	}
}

func TestExtractErrString(t *testing.T) {

	ass := assert.New(t)
	urlErr := &url.Error{
		Op:  "TestError",
		URL: "fobar",
		Err: errors.New("test"),
	}

	ass.Equal(
		"{\"op\":\"TestError\",\"url\":\"fobar\",\"err\":\"test\"}",
		extractErrString(urlErr),
		"error from `*url.Error` is expected")

	ass.Equal(
		"test",
		extractErrString(errors.New("test")),
		"error string is expected")

}

type logCatch struct {
	lock sync.Mutex
	log  string
}

func (lc *logCatch) insert(log string) {
	lc.lock.Lock()
	defer lc.lock.Unlock()
	lc.log = log
}

func (lc *logCatch) get() string {
	lc.lock.Lock()
	defer lc.lock.Unlock()
	return lc.log
}

func TestPrintBodyWhenDiscarding(t *testing.T) {
	ass := assert.New(t)
	testServer := restclienttest.NewTestServer(t)
	defer testServer.Close()
	// simulate GET request with 200 http code and a response
	testServer.AppendHandler(
		http.MethodGet,
		"/json",
		restclienttest.TestRequest{
			Name:         "Success call",
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"success"}`),
		})

	client := testServer.Client()
	rclient := WrapClient(client)

	reqData := Req{
		Host:   testServer.URL,
		Path:   "/json",
		Method: http.MethodGet,
	}

	oldVal := printBodyRespOnError
	printBodyRespOnError = true
	defer func() { printBodyRespOnError = oldVal }()

	catcher := &logCatch{}

	fnGetLog := func(arg interface{}) {
		str, ok := arg.(string)
		if ok {
			if regexp.MustCompile(`\[\w*\]\s\w*\s-\s\d*\s:\w*`).MatchString(str) {
				catcher.insert(str)
			}
		}
	}

	gotap.Attach(fnGetLog)
	defer gotap.Detach(fnGetLog)

	ass.NoError(rclient.Bind(context.Background(), reqData, nil), "request should be no error")
	time.Sleep(time.Second * 1)
	ass.Equal(`[GET]  - 200 : {"status":"success"} `, catcher.get())
}

func TestHooks(t *testing.T) {

	ass := assert.New(t)

	testServer := restclienttest.NewTestServer(t)
	defer testServer.Close()
	// simulate hooks with `Call` action where all hooks not return error
	testServer.AppendHandler(
		http.MethodGet,
		"/json",
		restclienttest.TestRequest{
			Name:         "Success call",
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"application/json"},
			},
			ResponseBody: []byte(`{"status":"success"}`),
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return ass.Equal(headers.Get("Accept"), "application/json")
			},
		})

	client := testServer.Client()
	rclient := WrapClient(client)

	reqData := Req{
		Host:   testServer.URL,
		Path:   "/json",
		Method: http.MethodGet,
		BeforeReq: func(ctx context.Context, req *http.Request) error {
			req.Header.Set("Accept", "application/json")
			return nil
		},
		AfterResp: func(ctx context.Context, res *http.Response) error {
			if res.Header.Get("Content-Type") != "application/json" {
				return errors.New("response header not expected")
			}
			return nil
		},
	}

	respBuff, respErr := rclient.Call(context.Background(), reqData)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(respBuff, "response should be not empty")
	ass.Equal([]byte(`{"status":"success"}`), respBuff, "response expected")

	var resp interface{}

	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.NoError(respErr, "request should be no error")
	ass.NotEmpty(resp, "response should be not empty")
	ass.Equal(map[string]interface{}{"status": "success"}, resp, "response expected")

	testServer.AppendHandler(
		http.MethodGet,
		"/text",
		restclienttest.TestRequest{
			Name:         "Success call",
			ResponseCode: 200,
			ResponseHeader: map[string][]string{
				"Content-Type": {"plain/text"},
			},
			ResponseBody: []byte(`hello world`),
			HeaderTest: func(ass *assert.Assertions, headers http.Header) bool {
				return ass.Equal(headers.Get("Accept"), "application/json")
			},
		})

	reqData.Path = "/text"

	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.Error(respErr, "request should be error")
	ass.Empty(respBuff, "response should be empty")

	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.Error(respErr, "request should be error")
	ass.Empty(respBuff, "response should be empty")

	reqData.BeforeReq = func(ctx context.Context, req *http.Request) error {
		return errors.New("test")
	}

	respBuff, respErr = rclient.Call(context.Background(), reqData)
	ass.Error(respErr, "request should be error")
	ass.Empty(respBuff, "response should be empty")

	respErr = rclient.Bind(context.Background(), reqData, &resp)
	ass.Error(respErr, "request should be error")
	ass.Empty(respBuff, "response should be empty")

}
