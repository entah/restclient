// Copyright (c) 2023 Ragil Rynaldo Meyer. All rights reserved.

// Package restclient simplifying operation of call to REST endpoint.
package restclient

import (
	"bytes"
	"context"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"

	"gitlab.com/entah/fngo"
	"gitlab.com/entah/gotap"
)

// printBodyRespOnError set env var `RESCLIENT_PRINT_RESP` to `1` to tap (see `gitlab.com/entah/gotap`) body resp when discarding.
var printBodyRespOnError = os.Getenv("RESCLIENT_PRINT_RESP") == "1"

// strOrs return the first not empty string from left to right order.
var strOrs func(strs ...string) string

func init() {
	fngo.BindOrFunc(&strOrs)
}

// RestClientError error struct used.
type RestClientError struct {
	Message   string `json:"message"`
	Host      string `json:"host"`
	Path      string `json:"path"`
	Method    string `json:"method"`
	Step      string `json:"step"`
	Timestamp int64  `json:"timestamp"`
}

// Error return error string.
func (rce *RestClientError) Error() string {
	return rce.Message
}

// extractErrString extract error string.
// if `error` is `*url.Error return string formatted `%#v` of the error, call `err.Error` otherwise.
func extractErrString(err error) string {
	errURL, ok := err.(*url.Error)
	if ok {
		return fmt.Sprintf(`{"op":"%s","url":"%s","err":"%s"}`, errURL.Op, errURL.URL, errURL.Err)
	}
	return err.Error()
}

// Req request config.
type Req struct {
	Host        string
	Path        string
	Method      string
	ContentType string
	BearerToken string
	BearerGen   func() string
	Body        interface{}
	Non2xxError bool
	Headers     map[string][]string
	Queries     map[string][]string
	Params      map[string]string
	BeforeReq   func(ctx context.Context, req *http.Request) error
	AfterResp   func(ctx context.Context, res *http.Response) error
}

// IRestClient interface for exposed method.
type IRestClient interface {
	// Bind execute http request with `req` and bind response to `data`.
	// If `data` is nil, discard response body.
	// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
	Bind(ctx context.Context, req Req, data interface{}) error
	// BindWithCode like `Bind`, but return http status code too.
	// If `data` is nil, discard response body.
	// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
	BindWithCode(ctx context.Context, req Req, data interface{}) (int, error)
	// Call execute http request with `req` and return response as `[]byte`.
	// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
	Call(ctx context.Context, req Req) ([]byte, error)
	// CallWithCode like `Call`, but return http status code too,
	// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
	CallWithCode(ctx context.Context, req Req) ([]byte, int, error)
}

// RestClient implement `IRestClient`.
type RestClient struct {
	client *http.Client
}

// CreateBody return `io.Readed` with contents from `Req.Body`.
func (req *Req) CreateBody() (io.Reader, error) {
	if req.Body == nil {
		return nil, nil
	}

	switch t := req.Body.(type) {
	case io.Reader:
		return t, nil
	case []byte:
		return bytes.NewReader(t), nil
	case string:
		return strings.NewReader(t), nil
	default:
		buff, err := json.Marshal(&req.Body)
		if err != nil {
			return nil,
				&RestClientError{
					Host:      req.Host,
					Message:   err.Error(),
					Path:      req.Path,
					Method:    req.Method,
					Step:      "Req#CreateBody",
					Timestamp: time.Now().Unix(),
				}
		}
		return bytes.NewReader(buff), nil
	}
}

// reCleanDoubleSlash regexp to remove double slash on path.
var reCleanDoubleSlash = regexp.MustCompile(`//`)

// reCleanPathHolder regexp to remove path holder used to compile path params on path.
var reCleanPathHolder = regexp.MustCompile(`/(:?:.*)/?`)

// AttachParams attach path params from `Req.Params` to `path` if `path` is not empty string.
// if `path` is empty string, will use `Req.Path` instead.
func (req *Req) AttachParams(path string) string {
	result := strOrs(path, req.Path)
	for key, val := range req.Params {
		if val != "" {
			result = strings.ReplaceAll(result, ":"+key, fmt.Sprint(val))
		}
	}

	result = reCleanDoubleSlash.ReplaceAllString(result, "")
	result = reCleanPathHolder.ReplaceAllString(result, "")

	return result
}

// AttachQueries attach query params from `Req.Queries` to `path` if `path` is not empty string.
// if `path` is empty string, will use `Req.Path` instead.
func (req *Req) AttachQueries(path string) string {
	result := strOrs(path, req.Path)
	if len(req.Queries) < 1 {
		return result
	}
	return result + "?" + url.Values(req.Queries).Encode()
}

// CompletePath return complete path from `Req.Path`, `Req.Params`, `Req.Queries`.
func (req *Req) CompletePath() string {
	parsedURL := req.AttachParams("")
	parsedURL = req.AttachQueries(parsedURL)
	return parsedURL
}

// CompleteURL return complete URL from `Req.Host`, `Req.Path`, `Req.Params`, `Req.Queries`.
func (req *Req) CompleteURL() string {
	return req.Host + req.CompletePath()
}

// bindRespBody unmarshall and bind response body to `data` based on header `Content-Type`.
// Known content type is `json` and `xml`.
// If `data` is empty, discard the response body.
// If content type unhandled, discard the response body.
// Will close response body when returning.
func (rc *RestClient) bindRespBody(data interface{}, resp *http.Response) error {
	if data == nil {
		rc.discardBodyResp(resp)
		return nil
	}

	buff, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		defer resp.Body.Close()
		return err
	}

	contentType := resp.Header.Get("Content-Type")

	switch {
	case strings.Contains(contentType, "json"):
		return json.Unmarshal(buff, &data)
	case strings.Contains(contentType, "xml"):
		return xml.Unmarshal(buff, &data)
	default:
		rc.discardBodyResp(resp)
		return nil
	}
}

// createRequest create `http.Request` based on filled field in `Req`.
func (rc *RestClient) createRequest(ctx context.Context, req Req) (*http.Request, error) {
	payload, err := req.CreateBody()

	if err != nil {
		return nil, err
	}

	parsedURL := req.CompleteURL()

	request, err := http.NewRequestWithContext(ctx, req.Method, parsedURL, payload)
	if err != nil {
		return nil, err
	}

	mergeHeader(request.Header, req.Headers)

	if req.ContentType != "" {
		request.Header.Set("Content-Type", req.ContentType)
	}

	if request.Header.Get("Authorization") != "" {
		return request, nil
	}

	if req.BearerToken != "" {
		request.Header.Set("Authorization", "Bearer "+req.BearerToken)
		return request, nil
	}

	if req.BearerGen != nil {
		request.Header.Set("Authorization", "Bearer "+req.BearerGen())
		return request, nil
	}

	return request, nil
}

// discardBodyResp discard response body and close the response.
func (rc *RestClient) discardBodyResp(resp *http.Response) {
	defer resp.Body.Close()
	if printBodyRespOnError {
		buff, err := ioutil.ReadAll(resp.Body)
		tapIfError(err)
		gotap.Tap(fmt.Sprintf("[%s] %s - %d : %s ", resp.Request.Method, resp.Request.URL.RawPath, resp.StatusCode, string(buff)))
		return
	}

	_, err := io.Copy(io.Discard, resp.Body)
	tapIfError(err)
}

// handleStatusCode handling `Req.Non2xxError` based on server http code response.
// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
func (rc *RestClient) handleStatusCode(non2xx bool, resp *http.Response) error {
	if !non2xx {
		return nil
	}

	if resp.StatusCode >= 200 && resp.StatusCode <= 299 {
		return nil
	}

	rc.discardBodyResp(resp)
	return fmt.Errorf("received status code %v while expect 2xx", resp.StatusCode)
}

// Bind execute http request with `req` and bind response to `data`.
// If `data` is nil, discard response body.
// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
func (rc *RestClient) Bind(ctx context.Context, req Req, data interface{}) error {
	_, err := rc.BindWithCode(ctx, req, data)
	return err
}

// BindWithCode like `Bind`, but return http status code too.
// If `data` is nil, discard response body.
// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
func (rc *RestClient) BindWithCode(ctx context.Context, req Req, data interface{}) (int, error) {
	preparedErr := &RestClientError{
		Host:   req.Host,
		Path:   req.Path,
		Method: req.Method,
	}

	request, err := rc.createRequest(ctx, req)
	if err != nil {
		preparedErr.Message = err.Error()
		preparedErr.Step = "RestClient#Bind#createRequest"
		preparedErr.Timestamp = time.Now().Unix()
		return 0, preparedErr
	}

	if req.BeforeReq != nil {
		if err := req.BeforeReq(ctx, request); err != nil {
			preparedErr.Message = err.Error()
			preparedErr.Step = "RestClient#Bind#BeforeReq"
			preparedErr.Timestamp = time.Now().Unix()
			return 0, preparedErr
		}
	}

	resp, err := rc.client.Do(request)
	if err != nil {
		preparedErr.Message = extractErrString(err)
		preparedErr.Step = "RestClient#Bind#doRequest"
		preparedErr.Timestamp = time.Now().Unix()
		return 0, preparedErr
	}

	if req.AfterResp != nil {
		if err := req.AfterResp(ctx, resp); err != nil {
			rc.discardBodyResp(resp)
			preparedErr.Message = err.Error()
			preparedErr.Step = "RestClient#Bind#AfterResp"
			preparedErr.Timestamp = time.Now().Unix()
			return 0, preparedErr
		}
	}

	if err := rc.handleStatusCode(req.Non2xxError, resp); err != nil {
		preparedErr.Message = err.Error()
		preparedErr.Step = "RestClient#Bind#handleStatusCode"
		preparedErr.Timestamp = time.Now().Unix()
		return 0, preparedErr
	}

	if err := rc.bindRespBody(data, resp); err != nil {
		preparedErr.Message = err.Error()
		preparedErr.Step = "RestClient#Bind#bindRespBody"
		preparedErr.Timestamp = time.Now().Unix()
		return 0, preparedErr
	}

	return resp.StatusCode, nil
}

// Call execute http request with `req` and return response as `[]byte`.
// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
func (rc *RestClient) Call(ctx context.Context, req Req) ([]byte, error) {
	buff, _, err := rc.CallWithCode(ctx, req)
	return buff, err
}

// CallWithCode like `Call`, but return http status code too,
// If `Req.Non2xxError` set to true and server respond with non 2xx http code, discard response body.
func (rc *RestClient) CallWithCode(ctx context.Context, req Req) ([]byte, int, error) {
	preparedErr := &RestClientError{
		Host:   req.Host,
		Path:   req.Path,
		Method: req.Method,
	}

	request, err := rc.createRequest(ctx, req)
	if err != nil {
		preparedErr.Message = err.Error()
		preparedErr.Step = "RestClient#Call#createRequest"
		preparedErr.Timestamp = time.Now().Unix()
		return nil, 0, preparedErr
	}

	if req.BeforeReq != nil {
		if err := req.BeforeReq(ctx, request); err != nil {
			preparedErr.Message = err.Error()
			preparedErr.Step = "RestClient#Call#BeforeReq"
			preparedErr.Timestamp = time.Now().Unix()
			return nil, 0, preparedErr
		}
	}

	resp, err := rc.client.Do(request)
	if err != nil {
		preparedErr.Message = extractErrString(err)
		preparedErr.Step = "RestClient#Call#doRequest"
		preparedErr.Timestamp = time.Now().Unix()
		return nil, 0, preparedErr
	}

	if req.AfterResp != nil {
		if err := req.AfterResp(ctx, resp); err != nil {
			rc.discardBodyResp(resp)
			preparedErr.Message = err.Error()
			preparedErr.Step = "RestClient#Call#AfterResp"
			preparedErr.Timestamp = time.Now().Unix()
			return nil, 0, preparedErr
		}
	}

	if err := rc.handleStatusCode(req.Non2xxError, resp); err != nil {
		preparedErr.Message = err.Error()
		preparedErr.Step = "RestClient#Call#handleStatusCode"
		preparedErr.Timestamp = time.Now().Unix()
		return nil, 0, preparedErr
	}

	defer resp.Body.Close()

	buff, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		preparedErr.Message = err.Error()
		preparedErr.Step = "RestClient#Call#readRespBody"
		preparedErr.Timestamp = time.Now().Unix()
		return nil, 0, preparedErr
	}

	return buff, resp.StatusCode, nil
}

// WrapClient create a new `RestClient` implement `IRestClient`.
func WrapClient(client *http.Client) IRestClient {
	return &RestClient{client: client}
}

// mergeHeader merge all value from `h2` to `h1`.
// If key header `h2` exists in `h1` will call `h1.Add` method, `h2.Set` otherwise.
func mergeHeader(h1, h2 http.Header) {
	for key, val := range h2 {
		headerExists := h1.Get(key) != ""
		if len(val) > 1 {
			for i, v := range val {
				if !headerExists && i == 0 {
					h1.Set(key, v)
					continue
				}
				h1.Add(key, v)
			}
			continue
		}

		if headerExists {
			h1.Add(key, val[0])
			continue
		}
		h1.Set(key, val[0])
	}
}

// tapIfError tap an error if error is not nil.
func tapIfError(err error) {
	if err != nil {
		gotap.Tap(err)
	}
}
