module gitlab.com/entah/restclient

go 1.20

require (
	github.com/stretchr/testify v1.8.4
	gitlab.com/entah/fngo v0.0.1
	gitlab.com/entah/gotap v0.0.1
	gitlab.com/entah/restclienttest v0.0.0-20230702132502-50e3e38e981b
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
